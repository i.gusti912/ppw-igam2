from django.db import models
from django.urls import reverse

# Create your models here.
class Matkul(models.Model):
    nama = models.CharField(max_length=20)
    dosen = models.CharField(max_length=20)
    sks = models.IntegerField()
    smesterTahun = models.CharField(max_length=20)
    kelas = models.CharField(max_length=20)
    deskripsi = models.TextField()

    def get_absolute_url(self):
        return reverse('mataKuliah')