from typing import Type
from django import forms
from django.db.models.base import Model
from django.forms import fields

from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = [
            'nama',
            'sks',
            'dosen',
            'smesterTahun',
            'kelas',
            'deskripsi'
        ]

        # widgets = {
        #     'nama': forms.TextInput(attrs={'class': 'form-control'}),
        #     'sks': forms.IntegerField(attrs={'class': 'form-control'}),
        #     'dosen': forms.TextInput(attrs={'class': 'form-control'}),
        #     'smesterTahun': forms.TextInput(attrs={'class': 'form-control'}),
        #     'kelas': forms.TextInput(attrs={'class': 'form-control'}),
        #     'deskripsi': forms.Textarea(attrs={'class': 'form-control'})
        # }