from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import ListView, DetailView, DeleteView
from django.views.generic.base import View

from .models import Matkul
from .forms import MatkulForm

def home(request):
    return render(request, 'main/halamanHome.html')

def verStory1(request):
    return render(request, 'main/verStory1.html')

# def mataKuliah(request):
#     return render(request, 'main/mataKuliah.html')

class mataKuliah(ListView):
    model = Matkul
    template_name = 'main/mataKuliah.html'

class mataKuliahDetail(DetailView):
    model = Matkul
    template_name = 'main/mataKuliahDetail.html'

def mataKuliahForm(request):
    form = MatkulForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = MatkulForm()
    
    context = {
        'form': form
    }
    return render(request, 'main/mataKuliahForm.html', context)

# def mataKuliahDelete(request):
#     Matkul.objects.all.delete()
#     return redirect('main/mataKuliah.html')  

class mataKuliahDelete(DeleteView):
    model = Matkul
    template_name = 'main/mataKuliahDelete.html'
    success_url = reverse_lazy('main:mataKuliah')

    # def get_success_url(self):
    #     return reverse('mataKuliah')
    