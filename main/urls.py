from django.urls import path

from . import views
from .views import mataKuliah, mataKuliahDetail, mataKuliahDelete

app_name = 'main'

urlpatterns = [
    path('', views.home, name='halamanHome'),
    path('verStory1/', views.verStory1, name='verStory1'),
    path('mataKuliahForm/', views.mataKuliahForm, name='mataKuliahForm'),
    path('mataKuliah/', mataKuliah.as_view(), name='mataKuliah'),
    path('mataKuliahDetail/<int:pk>', mataKuliahDetail.as_view(), name='mataKuliahDetail'),
    path('mataKuliahDetail/<int:pk>/hapus', mataKuliahDelete.as_view(), name='mataKuliahDelete'),
    # path('mataKuliahDelete/<matkul_id>', views.mataKuliahDelete, name='mataKuliahDelete'),
]
